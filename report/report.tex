\documentclass[a4paper]{article}
\usepackage{amsmath}
\usepackage{graphicx}
%\usepackage[text={7in,10in},centering]{geometry}

\setlength{\parindent}{0.0in}
\setlength{\parskip}{0.1in}

\title{Artificial Intelligence Programming\\
    \normalsize A general puzzle solver for Constraint Satisfaction Problems}
\author{Herman Schistad - MTDT}
\date{24.10.2013}
\begin{document}
\pagenumbering{roman}
\maketitle

\section{Description of code}

My code is organized in such a way that for a new puzzle to be added, no
modifications are needed to the algorithm logic. For a new puzzle to be
added I need to define:

\begin{itemize}
    \item How to represent conflicts / contraint violations
    \item How to find neighbour states
    \item Finding least conflicting state, among neighbour states
    \item Modifying a state/making a move.
    \item Printing a state
\end{itemize}

Much can be inferred by just looking at the code for our Min Conflicts
algorithm:

\begin{verbatim}
maxIterations := 10000
iteration := 0
for !(state.Solved()) && iteration != maxIterations {
    state = statemanager.NextMinConflictState() 
    iteraton++
}
return state
\end{verbatim}

This is a bit simplified from the original code. As it became evident
that the min conflicts algoritm often entered an osciallating state
between one or more states, which it by design never exits.

In order to solve this problem I propose a hybrid between Simulated
Annealing and Min Conflicts: if we've had over 50 rounds with the same
number of conflicts, we do something random - just chosing a neighbour.

As one can see in the verbatim above I have a concept of a $state$ and a
$statemanager$. Why the difference? Well, the statemanager is just an
abstraction or interface managing the logics of setting/getting states.
The statemanager is never changed during the run of an algorithm, but
its $state$ is both modified, added and removed multiple times.

The interface of a state (similar to a header of a C-file) describes
mandatory methods to describe a new puzzle:

\begin{verbatim}
type State interface {
    Solved() bool
    Evaluation() float64
    ToString() string
    TotalConflicts() int
    ConflictedStates() []int
}
\end{verbatim}

Similarily the interface of a statemanager looks like:

\begin{verbatim}
type StateManager interface {
    GetState() states.State
    SetState(states.State)
    NextMinConflictState() states.State
    GetRandomNeighbourStates(n int) []states.State
}
\end{verbatim}

Let us take a look at a high level diagram of the code:

\begin{figure}[h!]
    \includegraphics[width=1.0\textwidth]{code.jpg}
\end{figure}

As one can see, the algorithm only has a reference to the statemanager
and calls the states from there. Then it returns to main and we start
the process again.

\section{Sudoko puzzle - a description}

My chosen third puzzle for this exercice was 'Sudoku'. A very popular
puzzle with mainly four constraint representations:

\begin{enumerate}
    \item Unique numbers in all rows
    \item Unique numbers in all columns
    \item Unique numbers in all 'mini-grids'.
    \item Smallest number is 0 and largest is the length of one
    row/column.
\end{enumerate}

What makes these puzzles hard? In newspapers the size of the puzzle is
often k=3 (which means one mini-grid has a width of 3, or in other words
numbers in a row/column is $k^2 = 9$. They are then prefilled with
certain numbers set, and the rest up to the reader to fill. However, in
a local search we do the same precedure every time: fill all empty
tiles and calculate conflicts. How the prefilled numbers is set has
little to none influence on the computation time for a local search.
What makes the puzzle hard i our case is the \textbf{size of $k$}.

A simple evaluation function is chosen for Sudoku (and in general for
the other contraint problems as well): we count the number of total
conflicts/constraint violations and preffer states with less conflicts.

For the simulated annealing I've chosen the following constants:

\begin{itemize}
    \item Start temperature: 100
    \item Max iterations: 20 000
    \item Delta temperature: $temperature/1.001$
\end{itemize}

\section{Results - K-Queens Problem}

\begin{table}[h!]
    \begin{tabular}{ll|llllll}
    Algorithm & Difficulty    & Avg. eval & St. d. eval & Best eval & Avg. iter & St. d. iter & Lowest iter \\ \hline
    MC   & Easy (k=6)    & 36   & 0   & 36    & 62        & 92          & 3           \\
    SA   & Easy (k=6)    & 36   & 0   & 36    & 4446      & 1444        & 799         \\
    MC   & Medium (k=25) & 625  & 0   & 625   & 81        & 38          & 34          \\
    SA   & Medium (k=25) & 614  & 3   & 621   & 20 000    & 0           & 20 000      \\
    MC   & Hard (k=1000) & 100 000 & 0 & 100 000  & 712   & 40          & 670         \\
    SA   & Hard (k=1000) & -    & -   & -     & -         & -           & -           \\
    \end{tabular}
\end{table}

The hard scenerio demands too much CPU-power and takes too long time to
find a feasable solution with simulated annealing. It generates more states
and does more calculations in general, compared to the min conflicts algorithm.

\section{Results - Graph-coloring puzzle}

\begin{table}[h!]
    \begin{tabular}{ll|llllll}
    Algorithm & Difficulty & Avg. eval & St. d. eval & Best eval & Avg. iter & St. d. iter & Lowest iter \\ \hline
    MC   & Easy    & 100   & 0     & 100       & 28        & 9           & 15          \\
    SA   & Easy    & 100   & 0     & 100       & 8273      & 339         & 7389        \\
    MC   & Medium  & 100   & 0     & 100       & 83        & 32          & 36          \\
    SA   & Medium  & 100   & 0     & 100       & 10481     & 887         & 9078        \\
    MC   & Hard    & 100   & 0     & 100       & 694       & 83          & 558         \\
    SA   & Hard    & -     & -     & -         & -         & -           & -           \\
    \end{tabular}
\end{table}

The same can be said about the graph-coloring problem. Here the
Simulated Annealing does not manage to find solutions to the hard
problem.

\newpage

\section{Results - Suduko}

\begin{table}[h!]
    \begin{tabular}{ll|llllll}
    Algorithm & Difficulty & Avg. eval & St. d. eval & Best eval & Avg. iter & St. d. iter & Lowest iter \\ \hline
    MC   & Easy    & 48     & 0     & 48    & 22     & 11       & 3         \\
    SA   & Easy    & 48     & 0     & 48    & 5187   & 445      & 3797      \\
    MC   & Medium  & 648    & 0     & 648   & 18042  & 14788    & 753       \\
    SA   & Medium  & 613    & 0     & 621   & 20 000 & 0        & 20 000    \\
    MC   & Hard    & 3679   & 14    & 3707  & 10 000 & 0        & 10 000    \\
    SA   & Hard    & 3617   & 13    & 3644  & 20 000 & 0        & 20 000    \\
    \end{tabular}
\end{table}

Note that the maximum evaluation value obtainable in the hard puzzle is 3840.

\end{document}
