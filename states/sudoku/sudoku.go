package sudoku

import (
    "fmt"
    "strconv"
    "math"
    "math/rand"
)

type SudokuState struct {
    k int
    inputNumbers []int
    numbers []int
    conflicts []int
    cache map[int][]int
    miniGridSize int
}

func (state SudokuState) Solved() bool {
    for i := 0; i < len(state.conflicts); i++ {
        if (state.conflicts[i] != 0) {
            return false
        }
    }
    return true
}

func (state SudokuState) Evaluation() float64 {
    eval := 0
    h := float64(len(state.conflicts)*(state.k-1))
    if (state.Solved()) {
        return h
    }
    for i := 0; i < len(state.conflicts); i++ {
        eval += state.conflicts[i]
    }
    return h - float64(eval)
}

func (state SudokuState) Clone() SudokuState {
    newState := &SudokuState{
        k: state.k,
        inputNumbers: append([]int{}, state.inputNumbers...),
        numbers: append([]int{}, state.numbers...),
        conflicts: append([]int{}, state.conflicts...),
        miniGridSize: state.miniGridSize,
        cache: state.cache,
    }
    return *newState
}

func (state SudokuState) TotalConflicts() int {
    n := 0
    for i := 0; i < len(state.conflicts); i++ {
        n += state.conflicts[i]
    }
    return n
}

func (state SudokuState) LeastConflictedStates(index int) []int {
    leastConflicted := []int{}
    min := state.k * int(math.Sqrt(float64(state.k)))
    for i := 0; i < len(state.conflicts); i++ {
        // We ignore the position we are in currently.
        if (i != index) {
            // We also ignore those in our original state, they are inaccessible
            if (state.inputNumbers[i] == 0) {
                // Do temporary move (and thus calculate conflicts)
                state.SwapPieces(i, index)

                conflicts := state.conflicts[i]

                if (conflicts < min) {
                    min = conflicts
                    leastConflicted = []int{i}
                } else if (conflicts == min) {
                    leastConflicted = append(leastConflicted, i)
                }

                state.SwapPieces(index, i)
            }
        }
    }
    return leastConflicted
}

func removeDuplicatesAndMinus(a []int) []int {
    result := []int{}
    seen := map[int]int{}
    for _, val := range a {
        // remove zeroes
        if (val != -1) {
            if _, ok := seen[val]; !ok {
                result = append(result, val)
                seen[val] = val
            }
        }
    }
    return result
}

func (state SudokuState) getRelevantStates(index int) []int {
    // Returns a list of indexes, where each index represents a number relevant to check
    // when checking for constraint vialations.
    if _, ok := state.cache[index]; ok {
        return state.cache[index]
    }

    indexIds := make([]int, state.k * 3)
    for i := 0; i < len(indexIds); i++ {
        indexIds[i] = -1
    }
    k := 0

    // Horizontal
    rowNumber := index/state.k
    lowerBound := rowNumber * state.k
    upperBound := lowerBound + state.k
    for i := lowerBound; i < upperBound; i++ {
        if (i != index) {
            indexIds[k] = i
            k++
        }
    }

    // Vertical
    column := index % state.k
    for i := 0; i < state.k; i++ {
        id := (i * state.k) + column
        if (id != index) {
            indexIds[k] = id
            k++
        }
    }

    // Box
    diffFromRowZero := rowNumber % state.miniGridSize
    rowZero := rowNumber - diffFromRowZero

    // We only iterate through rows relevant for minigrid of the index.
    lowerBound = rowZero * state.k
    upperBound = state.miniGridSize * state.k + lowerBound

    // Column number floor
    columnFloor := column / state.miniGridSize

    for i := lowerBound; i < upperBound; i++ {
        // Are we in the same column group? 
        columnNumber := i % state.k
        currColumnFloor := columnNumber / state.miniGridSize
        if (currColumnFloor == columnFloor) {
            if (i != index) {
                indexIds[k] = i
                k++
            }
        }
    }

    cleanedIndexIds:= removeDuplicatesAndMinus(indexIds)
    state.cache[index] = cleanedIndexIds
    return cleanedIndexIds
}

func (state SudokuState) isConflict(index int, checkIdx int) bool {
    if (index != checkIdx) {
        if (state.numbers[index] == state.numbers[checkIdx]) {
            return true
        }
    }
    return false
}

func (state *SudokuState) initializeConflicts() {
    // Should just be called once (when initializing)
    for i := 0; i < len(state.numbers); i++ {
        if (state.inputNumbers[i] == 0) {
            relevantStates := state.getRelevantStates(i)
            for j := 0; j < len(relevantStates); j++ {
                if (state.isConflict(i, relevantStates[j])) {
                    state.conflicts[i]++
                }
            }
        }
    }
}

func (state SudokuState) RandomNeighbourState() SudokuState {
    newState := state.Clone()

    nonPreFilled := 0
    for i := 0; i < len(state.inputNumbers); i++ {
        if (state.inputNumbers[i] == 0) {
            nonPreFilled++
        }
    }

    nonPreFilledIndexes := make([]int, nonPreFilled)
    c := 0
    for i := 0; i < len(state.inputNumbers); i++ {
        if (state.inputNumbers[i] == 0) {
            nonPreFilledIndexes[c] = i
            c++
        }
    }

    index := nonPreFilledIndexes[rand.Intn(nonPreFilled)]
    newPos := nonPreFilledIndexes[rand.Intn(nonPreFilled)]

    newState.SwapPieces(index, newPos)
    return newState
}

func (state SudokuState) ConflictedStates() []int {
    conflicts := []int{}
    for i := 0; i < len(state.conflicts); i++ {
        if (state.conflicts[i] != 0) {
            conflicts = append(conflicts, i)
        }
    }
    return conflicts
}

func (state *SudokuState) addConflict(relevantStates []int, index, value int, cum bool) {
    for i := 0; i < len(relevantStates); i++ {
        // Is the index non-zero in our original puzzle?
        if state.isConflict(relevantStates[i], index) {
            if cum {
                state.conflicts[index] += value
            }
            if (state.inputNumbers[relevantStates[i]] == 0) {
                state.conflicts[relevantStates[i]] += value
            }
        }
    }
}

func (state *SudokuState) SwapPieces(index int, newIndex int) {
    // For each number related to index we want to move, we reduce conflicts with 1
    relIndex := state.getRelevantStates(index)
    relNewIndex := state.getRelevantStates(newIndex)

    state.addConflict(relIndex, index, -1, false)
    state.addConflict(relNewIndex, newIndex, -1, false)

    // Then we can reset the counter on index we want to move.
    state.conflicts[index] = 0
    state.conflicts[newIndex] = 0

    // Do the actual swap.
    t := state.numbers[index]
    state.numbers[index] = state.numbers[newIndex]
    state.numbers[newIndex] = t

    // Recalculate conflicts
    state.addConflict(relIndex, index, 1, true)
    state.addConflict(relNewIndex, newIndex, 1, true)
}

func (state SudokuState) prettifySudokuArray(numbers []int, zeroString string) string {
    s := ""
    for i := 0; i < len(numbers); i++ {
        if (numbers[i] > 0) {
            s = s + fmt.Sprintf("%v ", numbers[i])
        } else {
            s = s + fmt.Sprintf("%v ", zeroString)
        }
        newLine := (((i + 1) % state.k) == 0)
        gridSize := int(math.Sqrt(float64(state.k)))
        rowNumber := int(math.Floor(float64((i+1) / state.k)))
        gridRow := (rowNumber % gridSize == 0)

        // Print borders
        if ((i+1) % (state.k/gridSize) == 0 && !newLine) {
            s = s + "| "
        }
        // Print newline if end of line.
        if (newLine) {
            s = s + "\n"
            if (gridRow) {
                grids := state.k / gridSize
                for j := 0; j < grids-1; j++ {
                    s = s + "- - - + "
                }
                s = s + "- - -\n"
            }
        }
    }
    return s
}

func (state SudokuState) ToString() string {
    s := "-- Beginning state --\n"
    s = s + state.prettifySudokuArray(state.numbers, "-")
    s = s + fmt.Sprintf("-- Conflicts --\n")
    s = s + state.prettifySudokuArray(state.conflicts, "0")
    s = s + fmt.Sprintf("-- End of state --\n")
    return s
}

func parseGridString(numbers []int, k int) [][]int {
    // Empty 2d grid
    grid := make([][]int, k)
    for i := 0; i < len(grid); i++ {
        grid[i] = make([]int, k)
    }

    row := 0
    column := 0
    for i := 0; i < len(numbers); i++ {
        if (i % k == 0 && i > 0) {
            row += 1
            column = 0
        }
        grid[row][column] = numbers[i]
        column++
    }
    return grid
}

func NonNullIndex(array []int) int {
    split := rand.Intn(len(array))
    for i := split; i < len(array); i++ {
        if (array[i] != 0) {
            return i
        }
    }
    for i := 0; i < split; i++ {
        if (array[i] != 0) {
            return i
        }
    }
    return -1
}

func NewSudokuPuzzle(input string, k int) SudokuState {
    // Calculate size
    miniGridSize := k
    k = k * k

    // Parse the input string
    inputNumbers := make([]int, len(input) - 1)
    for i := 0; i < len(inputNumbers); i++ {
        inputNumbers[i], _ = strconv.Atoi(string(input[i]))
    }

    // Create a new array, containing local randomized state
    numbers := append([]int{}, inputNumbers...)

    // How many of the different values do we miss in our grid.
    missingNumbers := make([]int, k)
    for i := 0; i < k; i++ {
        missingNumbers[i] = k
    }

    // Count
    for i := 0; i < len(numbers); i++ {
        if (numbers[i] != 0) {
            missingNumbers[numbers[i]-1]--
        }
    }

    // And now from this array we fill the grid with random values.
    for i := 0; i < len(numbers); i++ {
        if (numbers[i] == 0) {
            // Find next missingNumber
            index := NonNullIndex(missingNumbers)
            nextNum := (index + 1)
            missingNumbers[index]--
            numbers[i] = nextNum
        }
    }

    // Allocate array with conflicts. Default to all values 0.
    conflicts := make([]int, len(numbers))

    // We fill the grid with numbers
    puzzle := new(SudokuState)
    puzzle.k = k
    puzzle.miniGridSize = miniGridSize
    puzzle.inputNumbers = inputNumbers
    puzzle.numbers = numbers
    puzzle.conflicts = conflicts
    puzzle.cache = make(map[int][]int)
    puzzle.initializeConflicts()
    return *puzzle
}
