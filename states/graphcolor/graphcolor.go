package graphcolor

import (
    "io/ioutil"
    "log"
    "strings"
    "strconv"
    "math/rand"
    "math"
    "fmt"
    "bytes"
    "github.com/ajstarks/svgo"
)

type GraphColorState struct {
    edges [][]int
    coordinates [][]float64
    nodeColors []string
    availableColors []string
    k int
    outfile string
}

func (state GraphColorState) TotalConflicts() int {
    conflicts := 0
    for i := 0; i < len(state.edges); i++ {
        if (state.nodeColors[state.edges[i][0]] == state.nodeColors[state.edges[i][1]]) {
            conflicts++
        }
    }
    return conflicts
}

func (state GraphColorState) Solved() bool {
    //  Check if two connecting vertexes has the same color
    for i := 0; i < len(state.edges); i++ {
        if (state.nodeColors[state.edges[i][0]] == state.nodeColors[state.edges[i][1]]) {
            return false
        }
    }
    return true
}

func (state GraphColorState) Evaluation() float64 {
    // Simple evaluation function. We count how many colors that
    // are different. A low number different colors means many
    // similar colors!
    // Worst case, all nodes have same color. (n-1)*n conflicts.
    if (state.Solved()){
        return 100
    }
    return float64(100 - state.TotalConflicts())
}

func (state GraphColorState) numberOfConflicts(vertex_id int) int {
    // Returns how many conflicts a given vertex is involved in.
    conflicts := 0
    for i := 0; i < len(state.edges); i++ {
        v1 := state.edges[i][0]
        v2 := state.edges[i][1]
        if (v1 == vertex_id || v2 == vertex_id) {
            if (state.nodeColors[v1] == state.nodeColors[v2]) {
                conflicts++
            }
        }
    }
    return conflicts
}

func (state GraphColorState) Clone() GraphColorState {
    newState := &GraphColorState{
        edges: append([][]int{}, state.edges...),
        coordinates: append([][]float64{}, state.coordinates...),
        nodeColors: append([]string{}, state.nodeColors...),
        availableColors: append([]string{}, state.availableColors...),
        k: state.k,
        outfile: state.outfile,
    }
    return *newState
}

func (state GraphColorState) RandomNeighbourState() GraphColorState {
    newState := state.Clone()

    vertex := rand.Intn(len(state.coordinates))

    color := state.availableColors[rand.Intn(len(state.availableColors))]
    newState.SetNewColor(vertex, color)

    return newState
}

func (state GraphColorState) LeastConflictedColors(vertex_id int) []string {
    // Given a vertex id, we want to check how many conflicts for each
    // color possibility.
    colors := []string{}
    min := len(state.coordinates)

    for i := 0; i < len(state.availableColors); i++ {
        // We only want to change color, if it is different from current
        if (state.availableColors[i] != state.nodeColors[vertex_id]) {
            oldColor := state.nodeColors[vertex_id]

            // Change the color temporarily
            state.SetNewColor(vertex_id, state.availableColors[i])

            // Count conflicts with this color
            nbConflicts := state.numberOfConflicts(vertex_id)

            // Change back the color
            state.SetNewColor(vertex_id, oldColor)

            if nbConflicts < min {
                // If the conflicts is lowest so far we create new list
                colors = []string{state.availableColors[i]}
                min = nbConflicts
            } else if (nbConflicts == min) {
                // If it is equally good, we add to list
                colors = append(colors, state.availableColors[i])
            }
        }
    }
    return colors
}

func (state GraphColorState) ConflictedStates() []int {
    // Loop through each vertex and count conflicts
    conflicts := []int{}
    for i := 0; i < len(state.coordinates); i++ {
        // We are only interested in states involved in > 0 conflicts
        if (state.numberOfConflicts(i) > 0) {
            conflicts = append(conflicts, i)
        }
    }
    return conflicts
}

func (state GraphColorState) SetNewColor(vertex_id int, color string) {
    state.nodeColors[vertex_id] = color
}

func (state GraphColorState) ToString() string {
    var buffer bytes.Buffer
    canvas := svg.New(&buffer)
    multiplier := float64(10)
    n_width := 5

    // Find lowest and highest coordinates
    lowest_x := float64(0)
    lowest_y := float64(0)
    highest_x := float64(0)
    highest_y := float64(0)
    for i := 0; i < len(state.coordinates); i++ {
        x_coord := state.coordinates[i][0]
        y_coord := state.coordinates[i][1]
        if (x_coord < lowest_x) {
            lowest_x = x_coord
        }
        if (y_coord < lowest_y) {
            lowest_y = y_coord
        }
        if (x_coord > highest_x) {
            highest_x = x_coord
        }
        if (y_coord > highest_y) {
            highest_y = y_coord
        }
    }

    width := int((highest_x - lowest_x) * multiplier) + 30
    height := int((highest_y - lowest_y) * multiplier) + 30

    canvas.Start(width, height)

    x_translate := int(math.Abs(lowest_x) * multiplier) + 5
    y_translate := int(math.Abs(lowest_y) * multiplier) + 25

    canvas.Translate(x_translate,y_translate)

    // Place all edges between the vertices
    for i := 0; i < len(state.edges); i++ {
        node_i := state.edges[i][0]
        node_ii := state.edges[i][1]
        x_i := state.coordinates[node_i][0]
        y_i := state.coordinates[node_i][1]
        x_ii := state.coordinates[node_ii][0]
        y_ii := state.coordinates[node_ii][1]
        canvas.Line(int(x_i * multiplier),
                    int(y_i * multiplier),
                    int(x_ii * multiplier),
                    int(y_ii * multiplier),
                    "stroke:black")
    }

    // Iterate through each vertex and place them
    for i := 0; i<len(state.coordinates); i++ {
        x := state.coordinates[i][0]
        y := state.coordinates[i][1]
        canvas.Circle(int(x * multiplier),
                      int(y * multiplier),
                      n_width,
                      "fill:" + state.nodeColors[i])
    }

    // Print legend with colors
    legend_x := -x_translate + 5
    legend_y := -y_translate + 5
    for i := 0; i < state.k; i++ {
        canvas.Rect(legend_x,
                    legend_y,
                    10,
                    10,
                    "fill:" + state.availableColors[i])
        canvas.Text(legend_x + 15,
                    legend_y + 9,
                    state.availableColors[i],
                    "fill: black; font-size: 12px")
        legend_x += 70
    }

    canvas.End()

    err := ioutil.WriteFile(state.outfile, []byte(buffer.String()), 0644)
    if (err != nil) { fmt.Println(err) }

    return fmt.Sprintf("Wrote to file %v\n", state.outfile)
}

func randomHex() string {
    return fmt.Sprintf("%x", rand.Intn(16777215))
}

func NewGraphColorState(k int, filename string, randomColors bool, outfile string) GraphColorState {
    preDefinedColors := []string{
        "EF4444", // red
        "FAA31B", // orange
        "394BA0", // dark blue
        "82C341", // green
        "009F75", // dark green
        "88C6ED", // light blue
        "FFF000", // yellow
        "D54799", // pink
    }

    // Read the problem file and add each line to a string array
    content, err := ioutil.ReadFile(filename)
    if err != nil {
        log.Fatal("Could not graph color file")
    }
    lines := strings.Split(string(content), "\n")

    // Split them further down by spaces
    s_lines := make([][]string, len(lines))
    for i :=0; i < len(lines); i++ {
        con := strings.Split(lines[i], " ")
        s_lines[i] = con
    }

    // We can now start to process the file, according to its spec
    vertices, _ := strconv.Atoi(s_lines[0][0])
    edges, _ := strconv.Atoi(s_lines[0][1])

    // Process all coordinates
    coordinates := make([][]float64, vertices)
    for i:=1;i<vertices+1;i++ {
        index, _ := strconv.Atoi(s_lines[i][0])
        x, _ := strconv.ParseFloat(s_lines[i][1], 64)
        y, _ := strconv.ParseFloat(s_lines[i][2], 64)
        coordinates[index] = []float64{x, y}
    }

    // Process all edges
    edge_array := make([][]int, edges)
    c := 0
    for i:=vertices+1;i<vertices + edges + 1;i++{
        index, _ := strconv.Atoi(s_lines[i][0])
        connected_node, _ := strconv.Atoi(s_lines[i][1])
        edge_array[c] = []int{index, connected_node}
        c++
    }

    availableColors := make([]string, k)
    if (randomColors || k >= 8) {
        // Create k random colors
        for i := 0; i < k; i++ {
            availableColors[i] = "#" + randomHex()
        }
    } else {
        // Colors is pre-defined.
        for i := 0; i < k; i++ {
            availableColors[i] = "#" + preDefinedColors[i]
        }
    }

    s := new(GraphColorState)
    s.k = k
    s.coordinates = coordinates
    s.edges = edge_array
    s.availableColors = availableColors
    s.outfile = outfile

    // Give every vertex a color of black, as default
    colors := make([]string, len(coordinates))
    for i := 0; i < len(coordinates); i++ {
       colors[i] = availableColors[rand.Intn(k)]
    }
    s.nodeColors = colors
    return *s
}
