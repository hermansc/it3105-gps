package states

type State interface {
    Solved() bool
    Evaluation() float64
    ToString() string
    TotalConflicts() int
    ConflictedStates() []int
}
