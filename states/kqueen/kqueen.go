package kqueen

import (
    "fmt"
    "math"
    "math/rand"
    "log"
)

type KQueenState struct {
    solved bool

    // internal
    k int
    // Array, with k queens. Value indicates column the queens sits in.
    queens []int
    // Each of these queens has a number of conflicts per row.
    conflicts []int
}

func (s KQueenState) Solved() bool {
    for i := 0; i < s.k; i++ {
        if (s.conflicts[i] != 0) {
            return false
        }
    }
    return true
}

func EmptyTestBoard() KQueenState {
    s := EmptyKQueenBoard(6)
    s.k = 6
    s.queens = []int{5,3,1,4,2,0}
    s.CalculateConflicts()
    return s
}

func EmptyKQueenBoard(k int) KQueenState {
    // Creates an empty state with all values set to reasonable defaults.
    s := new(KQueenState)
    s.k = k
    s.conflicts = make([]int, k)
    s.queens = make([]int, k)
    s.solved = false

    // Randomize positions
    for i := 0; i < k; i++ {
        s.queens[i] = rand.Intn(k)
    }

    // Calculate and set the conflicts
    s.CalculateConflicts()

    return *s
}

func (state KQueenState) TotalConflicts() int {
    n := 0
    for i := 0; i < len(state.conflicts); i++ {
        n += state.conflicts[i]
    }
    return n
}

func inArray(n int, array []int) bool {
    for i := 0; i < len(array); i++ {
        if array[i] == n {
            return true
        }
    }
    return false
}

func (state KQueenState) isConflict(i, j int) bool {
    // If row(queen_i) is in { row(queen_j),
    //                         row(queen_j) + | j - i |, 
    //                         row(queen_j) - | j - i | }
    // Then we have a conflict.
    tempConflicts := make([]int, 3)
    row_i := state.queens[i]
    row_j := state.queens[j]
    tempConflicts[0] = row_j

    row_diff := int(math.Abs(float64( j - i )))
    tempConflicts[1] = row_j + row_diff
    tempConflicts[2] = row_j - row_diff

    if inArray(row_i, tempConflicts) {
        return true
    }
    return false
}

func (state KQueenState) numberOfConflicts(column int, position int) int {
    // Get number of conflicts that a queen at given column and position is
    // involved in
    count := 0

    // First modify state, temporarily
    oldPosition := state.queens[column]
    state.queens[column] = position

    // Iterate through all queens
    for i := 0; i < state.k; i++ {
        // We do not want to count conflicts with ourselves
        if (i != column) {
            if state.isConflict(column, i) {
                count++
            }
        }
    }
    state.queens[column] = oldPosition
    return count
}

func (state KQueenState) CalculateConflicts() {
    // For each queen, we check her against all other queens.

    // Start by resetting all old conflicts
    for i := 0; i < state.k; i++ {
        state.conflicts[i] = 0
    }

    // Then calculate the number of conflicts.
    for i := 0; i < state.k; i++ {
        for j := 0; j < state.k; j++ {
            // We don't wan't to check conflicts with ourselves
            if j != i {
                if state.isConflict(i, j) {
                    state.conflicts[i]++
                }
            }
        }
    }
}

func (state KQueenState) ConflictedStates() []int {
    states := []int{}
    for i := 0; i < state.k; i++ {
        if (state.conflicts[i] > 0) {
            states = append(states, i)
        }
    }
    if len(states) == 0 {
        log.Fatal("Got none conflicts, although puzzle is not solved")
    }
    return states
}

func (state KQueenState) LeastConflictedPositions(col int) []int {
    // We've chosen a column, so we test all positions to see which yields
    // the lowest number of future conflicts.
    leastConflicted := []int{}
    min := state.k
    for i := 0; i < state.k; i++ {
        // We ignore the position we are in right now
        if (state.queens[col] != i) {
            // Count the number of conflicts
            conflicts := state.numberOfConflicts(col, i)
            if conflicts < min {
                min = conflicts
                leastConflicted = []int{i}
            } else if conflicts == min {
                leastConflicted = append(leastConflicted, i)
            }
        }
    }
    return leastConflicted
}

func (state KQueenState) MoveQueen(queen int, newPos int) {
    //fmt.Printf("Moving queen %v to pos: %v\n", queen, newPos)
    // We decrease conflicts for every queen attacking in the old position
    for i := 0; i < state.k; i++ {
        if (queen != i) {
            if state.isConflict(queen, i) {
                state.conflicts[i]--
            }
        }
    }

    // Make the move.
    state.queens[queen] = newPos

    // And add for the queens in the new pos
    for i := 0; i < state.k; i++ {
        if (queen != i) {
            if state.isConflict(queen, i) {
                state.conflicts[i]++
            }
        }
    }

    // And calculate number of conflicts in our own state
    state.conflicts[queen] = state.numberOfConflicts(queen, state.queens[queen])

}

func (state KQueenState) Clone() KQueenState {
    newState := &KQueenState{
        queens: append([]int{}, state.queens...),
        conflicts: append([]int{}, state.conflicts...),
        k: state.k,
        solved: state.solved,
    }
    return *newState
}

func (state KQueenState) RandomNeighbourState() KQueenState {
    // Returns a random state
    newState := state.Clone()

    // Get a random queen / row
    queen := rand.Intn(state.k)

    // Set the queen at a random column
    pos := rand.Intn(state.k)
    newState.MoveQueen(queen, pos)

    return newState
}

func (state KQueenState) Evaluation() float64 {
    eval := 0
    if (state.Solved()) {
        return math.Pow(float64(state.k), 2.0)
    }
    for i := 0; i < state.k; i++ {
        eval += state.conflicts[i]
    }
    l := len(state.conflicts)
    return float64((l * l) - eval)
}

func (state KQueenState) ToString() string {
    // Basic representation of the board, as a string
    var board = make([][]string, state.k)
    for i := range board {
        board[i] = make([]string, state.k)
    }

    // Build the board
    for i := 0; i < state.k; i++ {
        for j := 0; j < state.k; j++ {
            board[i][j] = "-"
        }
    }

    // Set out the queens
    for i := 0; i < state.k; i++ {
        board[state.queens[i]][i] = "Q"
    }

    // Do the actual printing
    s := "Board:\n"
    for i := 0; i < state.k; i ++ {
        s = s + fmt.Sprintf("%v\n", board[i])
    }
    s = s + fmt.Sprintf("Conflicts: %v\n", state.conflicts)
    return s
}
