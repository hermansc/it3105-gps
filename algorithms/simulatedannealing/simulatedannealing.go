package simulatedannealing

import (
    "gps/statemanagers"
    "gps/algorithms"
    "gps/states"
    "fmt"
    "math/rand"
    "math"
)

type SimulatedAnnealing struct {
    StateManager statemanagers.StateManager
}

func (m SimulatedAnnealing) Solve() (states.State, int) {
    statemanager := m.StateManager
    maxIterations := 20000
    nrNeighbours := 40

    // Set T to it's starting value Tmax
    temp := float64(100)

    iteration := 0
    for ; iteration < maxIterations; iteration++ {

        // Eval to current state
        Fp := statemanager.GetState().Evaluation()

        // Generate n neighbours of p
        neighbours := statemanager.GetRandomNeighbourStates(nrNeighbours)

        // Find the neighbour with highest evaluation
        bestNeighbour := neighbours[0]
        bestEval := bestNeighbour.Evaluation()
        for i := 1; i < len(neighbours); i++ {
            curr := neighbours[i].Evaluation()
            if (curr > bestEval) {
                bestEval = curr
                bestNeighbour = neighbours[i]
            }
        }

        // Calculate q and p
        diff := math.Abs(float64(bestEval - Fp))
        q := diff / Fp
        res := math.Exp(-q/temp)
        p := math.Min(1.0, res)

        // Generate random number in range [0,1]
        x := rand.Float64()

        // Exploiting
        newState := bestNeighbour
        if (x <= p) {
            // Exploring
            //fmt.Printf("Exploring. x: %v, p: %v, res: %v, diff: %v\n", x, p, res, diff)
            newState = neighbours[rand.Intn(len(neighbours))]
        } else {
            //fmt.Printf("Ploiting. x: %v, p: %v, res: %v, temp: %v, diff: %v\n", x, p, res, temp, diff)
            //fmt.Printf("From (%v):\n%v\n", Fp, statemanager.GetState().ToString())
        }

        statemanager.SetState(newState)
        //if (x > p) {
            //fmt.Printf("To (%v):\n%v\n", bestEval, statemanager.GetState().ToString())
        //}

        // Reduce the temperature
        temp = temp / float64(1.001)

        if statemanager.GetState().Solved() {
            fmt.Printf("")
            return statemanager.GetState(), iteration
        }
    }

    //fmt.Println(statemanager.GetState().ToString())
    return statemanager.GetState(), iteration
}

func NewSimulatedAnnealing(manager statemanagers.StateManager) algorithms.ConstrainBasedLocalSearch {
    return SimulatedAnnealing{StateManager: manager}
}
