package algorithms

import (
    "gps/states"
)

type ConstrainBasedLocalSearch interface {
    Solve() (states.State, int)
}
