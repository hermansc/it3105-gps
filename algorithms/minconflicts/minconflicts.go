package minconflicts

import (
    "gps/statemanagers"
    "gps/algorithms"
    "gps/states"
    "math/rand"
)

type MinConflict struct {
    StateManager statemanagers.StateManager
}

func (m MinConflict) Solve() (states.State, int) {
    statemanager := m.StateManager
    state := statemanager.GetState()
    iteration := 0
    maxIterations := 10000
    conflictsLastRound := 0
    equalConflictsCount := 0

    for !(state.Solved()) && iteration != maxIterations {
        state = statemanager.NextMinConflictState()
        iteration++
        currentConflicts := state.TotalConflicts()
        if (conflictsLastRound == currentConflicts) {
            equalConflictsCount++
        }
        if (equalConflictsCount > 50) {
            // We have had over 50 rounds with equal conflicts..
            // Time to chose something random, to kick us out of oscillating state.
            randState := statemanager.GetRandomNeighbourStates(1)
            state = randState[rand.Intn(len(randState))]
            statemanager.SetState(state)
            equalConflictsCount = 0
        }
        conflictsLastRound = currentConflicts
    }
    return state, iteration
}

func NewMinConflicts(manager statemanagers.StateManager) algorithms.ConstrainBasedLocalSearch {
    return MinConflict{StateManager: manager}
}
