package main

import (
    "fmt"
    "log"
    "math/rand"
    "math"
    "time"
    "flag"
    "os"
    "strings"
    "runtime/pprof"

    "gps/statemanagers"
    "gps/algorithms"

    "gps/statemanagers/kqueenmanager"
    "gps/statemanagers/graphcolormanager"
    "gps/statemanagers/sudokumanager"

    "gps/algorithms/minconflicts"
    "gps/algorithms/simulatedannealing"
)

type Result struct {
    solved bool
    iterations int
    finalState string
    evaluation int
}

func main() {
    // Generate a new random seed
    rand.Seed( time.Now().UTC().UnixNano() )

    // Set up flags we accept
    var algorithm = flag.String("algorithm", "", "Select which algorithm to use. [minconflicts, simulatedannealing]")
    var problem = flag.String("problem", "", "Select type of problem. [kqueen, graphcolor, sudoku]")
    var queens = flag.Int("queens", 8, "How many queens in k-queens problem")
    var ssize = flag.Int("ssize", 9, "How many columns/rows is the sudoku board")
    var filename = flag.String("filename", "", "Graph problem filename")
    var randcolors = flag.Bool("randcolor", false, "Generate random hex colors for gc-problem")
    var numruns = flag.Int("numruns", 1, "How many runs to run")
    var verbose = flag.Int("verbose", 0, "Print more information, not just the result table.")
    var outfile = flag.String("outfile", "index.html", "File to write graph color to")
    var cpuprofile = flag.String("cpuprofile", "", "write memory profile to this file")
    flag.Parse()

    // Enable cpuprofiling
    // Usage: go tool pprof main filename.prof
    if (*cpuprofile != "") {
        f, err := os.Create(*cpuprofile)
        if err != nil {
            log.Fatal("Could create memory profile file")
        }
        pprof.StartCPUProfile(f)
        defer pprof.StopCPUProfile()
    }

    resultTable := make([]Result, *numruns)

    // How many = blocks shall we print, when we are complete.
    resolution := 100

    for i := 0; i < *numruns; i++ {
        statemanager := *new(statemanagers.StateManager)
        alg := *new(algorithms.ConstrainBasedLocalSearch)

        // Parse which problem we want to solve
        switch p := *problem; {
            case p == "kqueen" || p == "kq":
                statemanager = kqueenmanager.NewKQueenStateManager(*queens)
            case p == "graphcolor" || p == "gc":
                statemanager = graphcolormanager.NewGraphColorStateManager(4, *filename, *randcolors,*outfile)
            case p == "sudoku" || p == "s":
                statemanager = sudokumanager.NewSudokuStateManager(*ssize, *filename)
            default:
                flag.PrintDefaults()
                log.Fatal("Invalid problem type")
        }

        // Parse which algorithm to use
        if (*algorithm == "minconflicts" || *algorithm == "mc") {
            alg = minconflicts.NewMinConflicts(statemanager)
        } else if (*algorithm == "simulatedannealing" || *algorithm == "sa") {
            alg = simulatedannealing.NewSimulatedAnnealing(statemanager)
        } else {
            flag.PrintDefaults()
            log.Fatal("Invalid algoritm type")
        }

        // Run the program
        result, iterations := alg.Solve()

        // Save result
        resultTable[i] = Result{
            solved: result.Solved(),
            iterations: iterations,
            finalState:  result.ToString(),
            evaluation: int(result.Evaluation()),
        }

        // Print/update progressbar
        done := int(math.Floor((float64(i+1) / float64(*numruns)) * float64(resolution)))
        todo := resolution - done
        fmt.Printf("[%v%v]", strings.Repeat("=", done), strings.Repeat(" ", todo))
        if (i != *numruns - 1) {
            fmt.Printf("\r")
        } else {
            fmt.Printf("\r%v\n", strings.Repeat(" ", done + 4))
        }
    }

    if (*verbose >= 2) {
        // Give feedback to user
        for i := 0; i < *numruns; i++ {
            if resultTable[i].solved {
                fmt.Printf("Found solution after %v iterations\n", resultTable[i].iterations)
            } else {
                fmt.Printf("Did not find solution after %v iterations :(\n", resultTable[i].iterations)
            }
            fmt.Println(resultTable[i].finalState)
        }
    }

    // Print the result table.
    fmt.Printf("Algorithm: %v, Problem: %v\n", *algorithm, *problem)

    if (*verbose >= 1) {
        fmt.Printf("Run\tSolved\tIterations\n")
        for i := 0; i < *numruns; i++ {
            fmt.Printf("%v\t%v\t%v\n", i, resultTable[i].solved, resultTable[i].iterations)
        }
    }

    evals := make([]int, *numruns)
    iterations := make([]int, *numruns)
    for i, result := range resultTable {
        evals[i] = result.evaluation
        iterations[i] = result.iterations
    }

    // Average and standard deviation of the evaluation of the K best-of-run states
    avgEval := Average(evals)
    sdEval := StandardDeviation(evals, avgEval)

    //Highest evaluation found over all K runs.
    _, highestEval := MinMaxNumber(evals)

    // Average and standard deviation of the number of steps/iterations in all K runs.
    avgRun := Average(iterations)
    fmt.Println(iterations)
    t := []int{36026, 4340, 3642, 19482, 1969, 67462, 13840, 4420, 6140, 12263, 1243, 11926, 856, 20577, 23111, 10578, 22136, 5518, 12974, 16079}
    fmt.Printf("Result: %v. Average: %v\n", StandardDeviation(t, Average(t)), Average(t))
    sdRuns := StandardDeviation(iterations, avgRun)

    // The quickest that an optimal solution was found among all K runs.
    lowestRun, _ := MinMaxNumber(iterations)

    fmt.Printf("Runs\tAvg. eval\tSD. eval\tBest eval\tAvg. run\tSD. run\t\tLowest run\n")
    fmt.Printf("%v\t%v\t\t%v\t\t%v\t\t%v\t\t%v\t\t%v\n", *numruns, avgEval, sdEval, highestEval, avgRun, sdRuns, lowestRun)
}

func MinMaxNumber(array []int) (int, int) {
    lowest := int(^uint(0) >> 1) // highest int
    highest := - (lowest - 1)
    for _, num := range array {
        if num > highest {
            // Best evaluation found over all K runs
            highest = num
        }
        if num < lowest {
            lowest = num
        }
    }
    return lowest, highest
}

func Average(array []int) int {
    totEval := 0
    for _, num := range array {
        totEval += num
    }
    avgEval := totEval / len(array)
    return avgEval
}

func StandardDeviation(array []int, avg int) int {
    distances := float64(0)
    for _, num := range array {
        dist := float64(num - avg)
        factor := float64(2)
        distances += math.Pow(dist, factor)
    }
    sdEval := int(math.Sqrt(distances / float64(len(array))))
    return sdEval
}
