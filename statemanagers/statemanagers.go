package statemanagers

import (
    "gps/states"
)

type StateManager interface {
    GetState() states.State
    SetState(states.State)
    NextMinConflictState() states.State
    GetRandomNeighbourStates(n int) []states.State
}
