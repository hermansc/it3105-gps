package graphcolormanager

import (
    "gps/states"
    "gps/states/graphcolor"
    "math/rand"
)

type GraphColorStatemanager struct {
    State graphcolor.GraphColorState
}

func (manager GraphColorStatemanager) GetState() states.State {
    return manager.State
}

func (manager *GraphColorStatemanager) SetState(s states.State) {
    manager.State = s.(graphcolor.GraphColorState)
}

func (manager GraphColorStatemanager) NextMinConflictState() states.State {
    // Get list of conflicted states (where two neigbours have same color)
    conflicts := manager.State.ConflictedStates()

    // Chose one to give new color by random choice.
    vertex := conflicts[rand.Intn(len(conflicts))]

    // Which new colors gives fewest conflicts
    colors := manager.State.LeastConflictedColors(vertex)

    // Chose the color on random
    color := colors[rand.Intn(len(colors))]

    manager.State.SetNewColor(vertex, color)
    return manager.State
}

func (manager GraphColorStatemanager) GetRandomNeighbourStates(n int) []states.State {
    states := make([]states.State, n)
    for i := 0; i < n; i++ {
        states[i] = manager.State.RandomNeighbourState()
    }
    return states
}

func NewGraphColorStateManager(k int, filename string, randomColors bool, outfile string) *GraphColorStatemanager {
    s := graphcolor.NewGraphColorState(k, filename, randomColors, outfile)
    return &GraphColorStatemanager{State: s}
}
