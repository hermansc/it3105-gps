package kqueenmanager

import (
//    "fmt"
    "math/rand"
    "gps/states"
    "gps/states/kqueen"
)

type KQueenStatemanager struct {
    State kqueen.KQueenState
}

func (manager KQueenStatemanager) GetState() states.State {
    return manager.State
}

func (manager *KQueenStatemanager) SetState(s states.State) {
    manager.State = s.(kqueen.KQueenState)
}

func (manager KQueenStatemanager) NextMinConflictState() states.State {
    // Get list of conflicted states. Chose column randomly.
    // Using this column, we search for least conflicts in available moves.
    // We make the move and return the new state.
    conflicts := manager.State.ConflictedStates()
    column := conflicts[rand.Intn(len(conflicts))]
    leastConflictedPositions := manager.State.LeastConflictedPositions(column)
    position := leastConflictedPositions[rand.Intn(len(leastConflictedPositions))]
    manager.State.MoveQueen(column, position)
    return manager.State
}

func (manager KQueenStatemanager) GetRandomStartState() states.State {
    return manager.State
}

func (manager KQueenStatemanager) GetRandomNeighbourStates(n int) []states.State {
    states := make([]states.State, n)
    for i := 0; i < n; i++ {
        states[i] = manager.State.RandomNeighbourState()
    }
    return states
}

func NewKQueenStateManager(n int) *KQueenStatemanager {
    s := kqueen.EmptyKQueenBoard(n)
    //s := kqueen.EmptyTestBoard()
    return &KQueenStatemanager{State: s}
}
