package sudokumanager

import (
    "io/ioutil"
    "log"
    "math/rand"
    //"fmt"
    "gps/states"
    "gps/states/sudoku"
)

type SudokuStatemanager struct {
    State sudoku.SudokuState
}

func (manager SudokuStatemanager) GetState() states.State {
    return manager.State
}

func (manager *SudokuStatemanager) SetState(s states.State) {
    manager.State = s.(sudoku.SudokuState)
}

func (manager SudokuStatemanager) NextMinConflictState() states.State {
    // Get conflicted states, that is empty tiles or tiles conflicting with others.
    conflicts := manager.State.ConflictedStates()

    // Chose one of them (column) on random
    index := conflicts[rand.Intn(len(conflicts))]

    // In this column, what piece can we set to minimize the nb. conflicts?
    leastConflicted := manager.State.LeastConflictedStates(index)

    // Chose one of these positions on random.
    newIndex := leastConflicted[rand.Intn(len(leastConflicted))]

    //fmt.Println(manager.State.ToString())
    manager.State.SwapPieces(index, newIndex)
    return manager.State
}

func (manager SudokuStatemanager) GetRandomNeighbourStates(n int) []states.State {
    states := make([]states.State, n)
    for i := 0; i < n; i++ {
        states[i] = manager.State.RandomNeighbourState()
    }
    return states
}

func NewSudokuStateManager(k int, filename string) *SudokuStatemanager {
    // Read the problem file which consist of problem string
    content, err := ioutil.ReadFile(filename)
    if err != nil {
        log.Fatal("Could not sudoku problem file")
    }

    // Create the puzzle
    s := sudoku.NewSudokuPuzzle(string(content), k)
    return &SudokuStatemanager{State: s}
}
